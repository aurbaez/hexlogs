/*
 * Created on 06/01/2004
 */
package com.tbd.logs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Properties;


/**
 * 
 */
public class HexAppLog {

	private boolean logActividadEnabled = false;
	private boolean logErrorEnabled = false;
	private boolean logInfoEnabled = false;
	private String name = null;
	private PrintWriter logActividad = null;
	private PrintWriter logError = null;
	private PrintWriter logInfo = null;
	private final String TIPO_ACTIVIDAD = "actividad";
	private final String TIPO_ERROR = "error";
	private final String TIPO_INFO = "info";
	private static HexLogs padre = null;
	
	
	public HexAppLog(String nameP) {
		Properties parametros = new Properties();
		InputStream iS = null;
		name = nameP;
		padre = HexLogs.getInstance();
		try {
			iS = getClass().getResourceAsStream("/tbdlogs.properties");
//			iS = new FileInputStream(ConstantesLogs.PATH_PROPERTIES);
			parametros.load(iS);
			String strTmp = parametros.getProperty("actividad.enabled");
			strTmp = (strTmp == null)?"true":strTmp;
			logActividadEnabled = (strTmp.trim().equalsIgnoreCase("true"));
			strTmp = parametros.getProperty("error.enabled");
			strTmp = (strTmp == null)?"true":strTmp;
			logErrorEnabled = (strTmp.trim().equalsIgnoreCase("true"));
			strTmp = parametros.getProperty("info.enabled");
			strTmp = (strTmp == null)?"true":strTmp;
			logInfoEnabled = (strTmp.trim().equalsIgnoreCase("true"));	
		} catch (Exception ex) {
			System.err.println("HexLogs: "+new Date()+" Tomando parametros por defecto para la aplicacion: "+name);
			logActividadEnabled = true;
			logErrorEnabled = true;
			logInfoEnabled = true;		 
		}
		
		String logRoot = padre.getPath();
		if (logActividadEnabled) {
			String logDirectoryPath = logRoot + "/" +name+ "/" +TIPO_ACTIVIDAD;
			File directorio = new File(logDirectoryPath);
			if (!directorio.exists()) {
				directorio.mkdirs();
			}
			FileOutputStream fOS = null; 
			try {
				fOS = new FileOutputStream(logDirectoryPath+"/"+"logActividad"+padre.getCurrentDate()+".log",true);
				logActividad = new PrintWriter(fOS,true);
			} catch (Exception e) {
				e.printStackTrace();
				logActividad = new PrintWriter(System.out,true);
			}
		}
		if (logErrorEnabled) {
			String logDirectoryPath = logRoot + "/" +name+ "/" +TIPO_ERROR;
			File directorio = new File(logDirectoryPath);
			if (!directorio.exists()) {
				directorio.mkdirs();
			}
			FileOutputStream fOS = null; 
			try {
				fOS = new FileOutputStream(logDirectoryPath+"/"+"logError"+padre.getCurrentDate()+".log",true);
				logError = new PrintWriter(fOS,true);
			} catch (Exception e) {
				e.printStackTrace();
				logError = new PrintWriter(System.err,true);
			}
		}
		if (logInfoEnabled) {
			String logDirectoryPath = logRoot + "/" +name+ "/" +TIPO_INFO;
			File directorio = new File(logDirectoryPath);
			if (!directorio.exists()) {
				directorio.mkdirs();
			}
			FileOutputStream fOS = null; 
			try {
				fOS = new FileOutputStream(logDirectoryPath+"/"+"logInfo"+padre.getCurrentDate()+".log",true);
				logInfo = new PrintWriter(fOS,true);
			} catch (Exception e) {
				e.printStackTrace();
				logInfo = new PrintWriter(System.err,true);
			}
		}
		
	}
	
	private synchronized void updateLogFileNamesOnChangeDate() {
		String logRoot = padre.getPath();
		if (logActividadEnabled) {
			//Se cierra el log viejo
			try {
				logActividad.close();
			} catch (Exception ex) {
				
			}
			String logDirectoryPath = logRoot + "/" +name+ "/" +TIPO_ACTIVIDAD;
			File directorio = new File(logDirectoryPath);
			if (!directorio.exists()) {
				directorio.mkdirs();
			}
			FileOutputStream fOS = null; 
			try {
				fOS = new FileOutputStream(logDirectoryPath+"/"+"logActividad"+padre.getCurrentDate()+".log",true);
				logActividad = new PrintWriter(fOS,true);
			} catch (Exception e) {
				e.printStackTrace();
				logActividad = new PrintWriter(System.out,true);
			}
		}
		if (logErrorEnabled) {
			//Se cierra el log viejo
			try {
				logError.close();
			} catch (Exception ex) {
				
			}
			String logDirectoryPath = logRoot + "/" +name+ "/" +TIPO_ERROR;
			File directorio = new File(logDirectoryPath);
			if (!directorio.exists()) {
				directorio.mkdirs();
			}
			FileOutputStream fOS = null; 
			try {
				fOS = new FileOutputStream(logDirectoryPath+"/"+"logError"+padre.getCurrentDate()+".log",true);
				logError = new PrintWriter(fOS,true);
			} catch (Exception e) {
				e.printStackTrace();
				logError = new PrintWriter(System.err,true);
			}
		}
		if (logInfoEnabled) {
			//Se cierra el log viejo
			try {
				logInfo.close();
			} catch (Exception ex) {
				
			}
			String logDirectoryPath = logRoot + "/" +name+ "/" +TIPO_INFO;
			File directorio = new File(logDirectoryPath);
			if (!directorio.exists()) {
				directorio.mkdirs();
			}
			FileOutputStream fOS = null; 
			try {
				fOS = new FileOutputStream(logDirectoryPath+"/"+"logInfo"+padre.getCurrentDate()+".log",true);
				logInfo = new PrintWriter(fOS,true);
			} catch (Exception e) {
				e.printStackTrace();
				logInfo = new PrintWriter(System.out,true);
			}
		}
	}
	
	public synchronized void logActivityEvent(String evento) {
		if (logActividadEnabled) {
			if (!padre.isCurrentDay()) {
				updateLogFileNamesOnChangeDate();
			}
			logActividad.println("["+getHeader()+"]-> "+evento);	
		}	
	}
	
	public synchronized void logActivityEvent(Object source, String sourceType, String methodName, String message, Object parameters[]) {
		if (logErrorEnabled) {
			if (!padre.isCurrentDay()) {
					updateLogFileNamesOnChangeDate();
			}
			logActividad.println("["+getHeader()+"]-> "+proccessSource(source)+"|"+formatLogString(sourceType)+"|"+formatLogString(methodName)+"|"+formatLogString(message)+"|"+proccessParameters(parameters));
		}	
	}

	
	public synchronized void logErrorEvent(String evento) {
		if (logErrorEnabled) {
			if (!padre.isCurrentDay()) {
					updateLogFileNamesOnChangeDate();
			}
			logError.println("["+getHeader()+"]-> "+evento);
		}	
	}
	
	public synchronized void logErrorEvent(Object source, String sourceType, String methodName, Exception ex, Object parameters[]) {
		if (logErrorEnabled) {
			if (!padre.isCurrentDay()) {
					updateLogFileNamesOnChangeDate();
			}
			logError.println("["+getHeader()+"]-> "+proccessSource(source)+"|"+formatLogString(sourceType)+"|"+formatLogString(methodName)+"|"+formatLogString(ex.toString())+"|"+formatLogString(ex.getStackTrace().toString())+"|"+proccessParameters(parameters));
		}	
	}

	public synchronized void logErrorEvent(Object source, String sourceType,String methodName, Exception ex) {
		if (logErrorEnabled) {
			if (!padre.isCurrentDay()) {
					updateLogFileNamesOnChangeDate();
			}
//			int i = 0;
			logError.println("["+getHeader()+"]-> "+proccessSource(source)+"|"+formatLogString(sourceType)+"|"+formatLogString(methodName)+"|"+formatLogString(ex.toString())+"|"+formatLogString(ex.getStackTrace().toString()));
		}	
	}
	
	private String proccessSource(Object source) {
		String result = null;
		source = (source == null)?new Object():source;
		result = source.getClass().getName();
		int index = result.lastIndexOf('.');
		index = (index == -1)?0:index;
		return result.substring(index);
	}
	
	private String proccessParameters(Object parameters[]) {
		String result = "";
		try {
			int numParams = parameters.length;
			Object param = null;
			for (int i = 0; i < numParams; i++) {
				param = parameters[i];
				result+=formatClassName(param.getClass().getName())+"="+param+";";
			}
			return result;
		} catch (Exception ex) {
			return "unparseable params";
		}
		
	}
	
	private String formatClassName(String className) {
		String result = className.getClass().getName();
		int index = result.lastIndexOf('.');
		index = (index == -1)?0:index;
		return result.substring(index);
	}
	
	private String formatLogString(String original) {
		return original.replace('|', '\\');
	}
	
	public synchronized void logInfoEvent(String evento) {
		if (logInfoEnabled) {
			if (!padre.isCurrentDay()) {
					updateLogFileNamesOnChangeDate();
			}
			logInfo.println("["+getHeader()+"]-> "+evento);
		}	
	}
	
	public synchronized void logInfoEvent(Object source, String sourceType, String methodName, String message, Object parameters[]) {
		if (logErrorEnabled) {
			if (!padre.isCurrentDay()) {
					updateLogFileNamesOnChangeDate();
			}
			logInfo.println("["+getHeader()+"]-> "+proccessSource(source)+"|"+formatLogString(sourceType)+"|"+formatLogString(methodName)+"|"+formatLogString(message)+"|"+proccessParameters(parameters));
		}	
	}

	
	private String getHeader() {
		String dateString = null;
		dateString = new Date().toString();	
		return dateString;
	}
	
	public synchronized void closeLogFiles() {
		if (logActividadEnabled) {
			logActividad.close();
		}
		if (logErrorEnabled) {
			logError.close();
		}
		if (logInfoEnabled) {
			logInfo.close();
		}
	}
}
