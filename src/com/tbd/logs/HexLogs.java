package com.tbd.logs;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

/**
 * 
 */
public class HexLogs {

	private static HexLogs instancia = null;
	private static HashMap<String, HexAppLog> logsMap = null;
	public Properties parametros = null;
	private static Calendar time = null;
	 
	
	
	public static HexLogs getInstance() {
		if (instancia == null) {
			instancia = new HexLogs();
		}
		return instancia;
	}
	
	public static HexLogs getInstance(String configFile) {
		if (instancia == null) {
			instancia = new HexLogs(configFile);
		}
		return instancia;
	}
	
	/**
	 * 
	 */
	private HexLogs() {
		if (logsMap == null) {
			//Se cargan los parametros de configuracion
			InputStream iS = null;
			parametros = new Properties();
			try {
				iS = getClass().getResourceAsStream("/tbdlogs.properties");	
				parametros.load(iS);
			} catch (Exception ex) {
				parametros = new Properties();
			} finally {
				try {
					if (iS!=null)
					iS.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			 logsMap = new HashMap<String, HexAppLog>();
			 System.out.println("Log: "+logsMap);
			 time = Calendar.getInstance();
			 time.set(Calendar.HOUR, 0);
			 time.set(Calendar.MINUTE, 0);
			 time.set(Calendar.SECOND, 0);
			 time.set(Calendar.AM_PM, Calendar.AM);
		}
	}
	
	/**
	 * 
	 */
	private HexLogs(String configFile) {
		if (logsMap == null) {
			//Se cargan los parametros de configuracion
			InputStream iS = null;
			parametros = new Properties();
			try {
				iS = getClass().getResourceAsStream("/"+configFile);
				parametros.load(iS);
			} catch (Exception ex) {
				parametros = new Properties();
			} finally {
				try {
					if (iS!=null)
					iS.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			 logsMap = new HashMap<String, HexAppLog>();
			 
			 time = Calendar.getInstance();
			 time.set(Calendar.HOUR, 0);
			 time.set(Calendar.MINUTE, 0);
			 time.set(Calendar.SECOND, 0);
			 time.set(Calendar.AM_PM, Calendar.AM);
		}
	}

	private HexLogs(InputStream configFile) {
		if (logsMap == null) {
			//Se cargan los parametros de configuracion
			InputStream iS = null;
			parametros = new Properties();
			try {
				iS = configFile;
				parametros.load(iS);
			} catch (Exception ex) {
				parametros = new Properties();
			} finally {
				try {
					if (iS!=null)
					iS.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			 logsMap = new HashMap<String, HexAppLog>();
			 
			 time = Calendar.getInstance();
			 time.set(Calendar.HOUR, 0);
			 time.set(Calendar.MINUTE, 0);
			 time.set(Calendar.SECOND, 0);
			 time.set(Calendar.AM_PM, Calendar.AM);
		}
	}

	
	public String getCurrentDate() {
		Calendar today = Calendar.getInstance();
		Calendar tomorrow = time;
		int intTmp = -1;
		tomorrow.add(Calendar.DAY_OF_MONTH, 1);
		if (!today.after(tomorrow)) {
			//Hay que actualizar el calendar
			today.set(Calendar.HOUR, 0);
			today.set(Calendar.MINUTE, 0);
			today.set(Calendar.SECOND, 0);
			today.set(Calendar.AM_PM, Calendar.AM);
			time = today;
		}
		String mes = null;
		String dia = null;
		String ano = null;
		intTmp = time.get(Calendar.DAY_OF_MONTH);
		dia = (intTmp < 10)?"0"+intTmp:""+intTmp;
		intTmp = time.get(Calendar.MONTH) + 1;
		mes = (intTmp < 10)?"0"+intTmp:""+intTmp;
		ano = ""+time.get(Calendar.YEAR);
		return dia+mes+ano;
	}
	
	public boolean isCurrentDay() {
		Calendar now = Calendar.getInstance();
		if (now.get(Calendar.DAY_OF_MONTH) != time.get(Calendar.DAY_OF_MONTH)) {
			return false;
		} else {
			return true;
		}
	}
	
	public String getPath() {
		String path = null;	
		path = parametros.getProperty("tbd.log.logRoot");
		path = (path == null)?"/server/www/logs":path; 
		return path;
	}
	
	public HexAppLog getLog(String key) {
		HexAppLog log = (HexAppLog) logsMap.get(key);
		if (log == null) {
			try {
				log = new HexAppLog(key);
				logsMap.put(key, log);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("HexLogs: "+new Date()+"AdministradorLog trabajando con parametros por defecto");
			}
		}
		return log;
	}
	
	public void closeApplicationLogs() {
		Iterator<String> iter = logsMap.keySet().iterator();
		while (iter.hasNext()) {
			HexAppLog log = (HexAppLog) logsMap.get(iter.next());
			log.closeLogFiles();
			logsMap.remove(iter.next());
		}
	}

	public Properties getParametros() {
		return parametros;
	}
}
