package com.tbd.logs;

/**
 * Clase que encapsula la funcionalidad de logging
 * regulandola por nivel de logging, configurable en el properties
 * 
 * @author Alvaro J Urbaez (Date: 07/12/2014)
 */
public class Logger {
	
	private byte loggingLevel = 3; //Por defecto escribe los tres niveles
	private HexAppLog log = null;
	private static Logger _instance = null;
	
	private Logger() {
		log = HexLogs.getInstance().getLog("hexlogs");
	}
	
	private Logger(String logName) {
		log = HexLogs.getInstance().getLog(logName);
		try {
			loggingLevel = Byte.parseByte(HexLogs.getInstance().getParametros().getProperty("tbd.log.logLevel"));
		} catch (Exception r) {
			System.err.println("LOGGER - Fallo la carga del properties de configuracion");
			loggingLevel = 3;
		}
	}
	
	private Logger(String logName, byte loggingLevel) {
		this.loggingLevel = loggingLevel;
		log = HexLogs.getInstance().getLog(logName);
	}
	
	private Logger(String logName, byte loggingLevel, String configFile) {
		this.loggingLevel = loggingLevel;
		log = HexLogs.getInstance(configFile).getLog(logName);
	}
	
	public static Logger getInstance() {
		if (_instance == null)
			_instance = new Logger();
		return _instance;
	}
	
	public static Logger getInstance(String log) {
		if (_instance == null)
			_instance = new Logger(log);
		return _instance;
	}
	
	public static Logger getInstance(String log, byte level) {
		if (_instance == null)
			_instance = new Logger(log, level);
		return _instance;
	}
	
	public static Logger getInstance(String log, byte level, String configFile) {
		if (_instance == null)
			_instance = new Logger(log, level, configFile);
		return _instance;
	}
	
	public void activity(String l) {
		if (loggingLevel > 2)
			log.logActivityEvent(l);
	}
	public void info(String l) {
		if (loggingLevel > 1)
			log.logInfoEvent(l);
	}
	public void error(String l) {
		if (loggingLevel > 0)
			log.logErrorEvent(l);
	}
}