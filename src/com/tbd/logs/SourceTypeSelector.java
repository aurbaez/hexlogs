package com.tbd.logs;

public class SourceTypeSelector {
	
		public static final String DAO = "DAO";
		public static final String SESSION_EJB = "SESSION_EJB";
		public static final String SESSION_FACADE = "SESSION_FACADE";
		public static final String ENTITY_EJB = "ENTITY_EJB";
		public static final String SERVICE_LOCATOR = "SERVICE_LOCATOR";
		public static final String MDB_EJB = "MDB_EJB";
		public static final String DB_SERVICES = "DB_SERVICES";
		public static final String STRUTS_ACTION = "ACTION";
		public static final String STRUTS_FORM = "STRUTS_FORM";
		public static final String PAGE = "PAGE";

}
