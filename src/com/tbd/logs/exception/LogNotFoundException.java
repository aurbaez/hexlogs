package com.tbd.logs.exception;

/**
 * 
 */
public class LogNotFoundException extends Exception {

	private static final long serialVersionUID = -1663235610592540746L;

	public LogNotFoundException() {
		super();
	}

	public LogNotFoundException(String arg0) {
		super(arg0);
	}

}
