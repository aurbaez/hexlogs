package com.tbd.logs.exception;

/**
 * 
 */
public class PropertyFileNotFoundException extends Exception {

	private static final long serialVersionUID = 3683728894802623016L;

	public PropertyFileNotFoundException() {
		super();
	}

	public PropertyFileNotFoundException(String arg0) {
		super(arg0);
	}

}
